#!/usr/bin/python3

import sys

show_heads = True

def summarise_data(serial_num:str, data_lines:list) -> None:
    global show_heads
    if show_heads:
        show_heads = False
        txt_tok = []
        for i in range(0,4):
            title = data_lines[0][16*i:16*(i+1)].strip().replace(" ","_")
            txt_tok.append(title)
        for i in range(0,4):
            title = data_lines[2][16*i:16*(i+1)].strip().replace(" ","_")
            txt_tok.append(title)
            
        txt = "              "
        for tok in txt_tok:
            txt = txt + "{:14}".format(tok)
        print(txt)

    l2 = data_lines[1].split()
    l4 = data_lines[3].split()
    data = l2 + l4
    txt = '"{}"'.format(serial_num) + "  "
    for tok in data:
        txt = txt + "{:14}".format(tok)
    print(txt)

def scan_file(filename: str)-> None:
    with open(filename, "r") as f:
        lines = f.readlines()
    look_for_serial_num = True
    useful_lines = 0
    data_lines = []

    for line in lines:
        if useful_lines > 0:
            data_lines.append(line)
            useful_lines -= 1
            if useful_lines == 0:
                summarise_data(serial_num, data_lines)

        if look_for_serial_num:
            tokens = line.split()
            if "0x10ee" not in tokens:
                continue
            serial_num = tokens[len(tokens)-1]
            look_for_serial_num = False
            data_lines = []
        else:
            if len(line) < 14:
                continue
            if line[0:14] == "Temperature(C)":
                useful_lines = 4
                look_for_serial_num = True

        



if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("missing filename argument")
        sys.exit(-1)
    print("scan '{}'".format(sys.argv[1]))
    scan_file(sys.argv[1])
