#!/usr/bin/python3

import sys

def lines_to_csv(filename):
    with open(filename, "r") as f:
        lines = f.readlines()
    for line in lines:
        line = line.strip()
        if len(line) > 0:
            csvline = ",".join(line.split())
            print(csvline)


if __name__ == "__main__":
    lines_to_csv(sys.argv[1])
